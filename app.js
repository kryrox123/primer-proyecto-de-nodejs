//importar colores
const argv = require('yargs').argv;
require('colors');

const { generarArchivo } = require('./helpers/multiplicar');


//  console.log(process.argv);
//  console.log(argv);

// console.log('base: yargs', argv.base);

  //const base = 7;
//console.log(process.argv);

// const [, , arg3 = 'base=9'] = process.argv;
// const [, base] = arg3.split('=');
// console.log(base);

 generarArchivo(argv.base)
   .then(nombreArchivo => console.log(nombreArchivo.yellow, 'creado'))
   .catch(err => console.log(err));